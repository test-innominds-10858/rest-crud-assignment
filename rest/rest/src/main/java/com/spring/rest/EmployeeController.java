package com.spring.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.rest.entity.Employee;
import com.spring.rest.entity.Employee;
import com.spring.rest.entity.User;
import com.spring.rest.repository.EmployeeRepository;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeRepository employeeRepo;
	
	@GetMapping("/employees")
	public List<Employee> getAllEmployees() {
		return employeeRepo.findAll();
	}
	
	

	@GetMapping("/employees/{id}")
	public Employee getOnEmployees(@PathVariable("id") Long id) {
		return employeeRepo.getById(id);
		
		
	}
	
	@PostMapping("/employees")
	public Employee addEmployees(@RequestBody Employee requestEmployees) {
		
		return employeeRepo.save(requestEmployees);
	}
	
	
	@PutMapping("/employees/{id}")
	public Employee updateEmployees(@PathVariable("id") Long id, @RequestBody Employee newEmployeesValue)
	{
		
		@SuppressWarnings("deprecation")
		Employee s = employeeRepo.getById(id);
		
		if(s!=null)
		{
			s.setFname(newEmployeesValue.getFname());
			s.setLname(newEmployeesValue.getLname());
			
			s.setEmail(newEmployeesValue.getEmail());
			s.setPhoneNumber(newEmployeesValue.getPhoneNumber());
			s.setHireDate(newEmployeesValue.getHireDate());
			s.setJob_id(newEmployeesValue.getJob_id());
			s.setSal(newEmployeesValue.getSal());
			s.setManager_id(newEmployeesValue.getManager_id());
		}
		return employeeRepo.save(s);
	
	
}
	
	@DeleteMapping("/employees/{id}")
	
	public String deleteEmployees(@PathVariable("id") Long id)
	{
		Employee s = employeeRepo.getById(id);
		
		if(s != null)
		{
			employeeRepo.delete(s);
			return "Employee deleted";
		}
		else
		{
			return "Employee not available";
		}
	
		
		
}
	
	
}