package com.spring.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring.rest.entity.User;

@RestController
public class UserController {
	
	static List<User> allUser = new ArrayList<>();
	static {
		
		User u1 = new User(1L, "ABC", "Hyderabad", 25);
		User u2 = new User(2L, "XYZ", "Chennai", 30);
		User u3 = new User(3L, "PQR", "Mumbai", 22);
		
		allUser.add(u1);
		allUser.add(u2);
		allUser.add(u3);
		
		
	}

	
	@GetMapping("/users")
	public List<User> getAllUser() {
		return allUser;
	}
	
	

	@GetMapping("/users/{id}")
	public User getOneUser(@PathVariable("id") Long id) {
		
		
		for(User u:allUser) {
			if(u.getId()== id) {
				return u;
			}
		}
		return null;
	}
	
	@PostMapping("/users")
	public User addUser(@RequestBody User requestUser) {
		
		allUser.add(requestUser);
		return requestUser;	
	}
	
	
	
	
	
}
